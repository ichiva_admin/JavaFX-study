package sample.DOC_01_06;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.text.Text;

/**
 * @Description
 * @auther Chiva 874460517@qq.com
 * @create 2019-12-31 15:02
 */
public class FXMLExampleController {
    @FXML
    private Text actiontarget;

    @FXML
    public void handleSubmitButtonAction(ActionEvent actionEvent) {
        actiontarget.setText("Sign in button pressed");
    }
}
